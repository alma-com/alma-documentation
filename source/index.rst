.. docs-v4 documentation master file, created by
   sphinx-quickstart on Sat Nov 14 11:38:45 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Документация ALMA.CMS v5
===========================================

.. toctree::
   :maxdepth: 2

   install
   .. view
   .. eloquent
   .. auth
   .. validator
   .. url
   .. js-ajax-form